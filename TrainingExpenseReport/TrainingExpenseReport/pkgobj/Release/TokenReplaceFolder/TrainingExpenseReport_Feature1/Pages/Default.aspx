﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="VB" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.12.4.min.js"></script>
   
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <meta name="WebPartPageExpansion" content="full" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />

    <!-- Add your JavaScript to the following file -->
    <script type="text/javascript" src="../Scripts/App.js"></script>
</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    <h4>OPC Agency FY Training Expense Report</h4>
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <p>Select fields to display on the report</p>
    <div class="container">
        <div class="section">
            <input id="ckbPerDiem" type="checkbox" name="report" value="Per Diem" />Per Diem<br />
            <input id="ckbReconciliation" type="checkbox" name="report" value="Reconciliation Report Adjustment" />Reconciliation Report Adjustment<br />
            <input id="ckbRegistration" type="checkbox" name="report" value="Registration Cost" />Registration Cost<br /> 
        </div>
        <div class="section">           
            <input id="ckbHotel" type="checkbox" name="report" value="Total Hotel Cost" />Total Hotel Cost<br />
            <input id="ckbTravel" type="checkbox" name="report" value="Cost of Mode of Travel" />Cost of Mode of Travel<br />
        </div>
        <div class="section">
            
        </div>
    </div>
    <p class="message clear">* To get a report for all employees for the Fiscal year to date, hit the submit button without entering values.</p>
    <div>
        <div class="column1 bold">Employee Name: </div>
        <div class="column2">
            <select id="lstDisplayName">
                <option value="Please select">Select Name</option>
            </select>
        </div>
    </div>
    <div class="column1 bold">Request created date</div>
    <div>
        <div class="column1">From: </div>
        <div class="column2">
            <input type="date" name="dtFrom" id="dtFrom" />
        </div>
    </div>
    <div>
        <div class="column1">To: </div>
        <div class="column2">
            <input type="date" name="dtTo" id="dtTo" />
        </div>
    </div>
    <div class="submit">
        <input id="Submit" type="button" value="Submit" />
    </div>
    <div>
        <p id="message" class="errorMessage">
            Please select a later date for Date to field.
        </p>
    </div>
</asp:Content>
