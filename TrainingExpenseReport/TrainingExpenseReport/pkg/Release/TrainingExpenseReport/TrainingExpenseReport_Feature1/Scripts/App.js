﻿var hostweburl;
var addinweburl;
var formDigestValue;
var reportContent;
var rows;
//Get values for each item in a field
//arrays to hold column values 
var perDiem;
var reconRepAdj;
var registrationCost;
var hotelCost;
var travelCost;
var totalCost;
var totalPerPerson;

$(document).ready(function () {
    hostweburl = decodeURIComponent(getQueryStringParameter("SPHostUrl"));
    addinweburl = decodeURIComponent(getQueryStringParameter("SPAppWebUrl"));
    var scriptbase = hostweburl + "/_layouts/15/";
    $.getScript(scriptbase + "SP.RequestExecutor.js", retrieveFormDigest);

    $('input#Submit').click(function () {
        if (($('input#dtFrom').val() != '' && $('input#dtTo').val() != '') && ($('input#dtFrom').val() > $('input#dtTo').val())) {
            $('#message').css("visibility", "visible")
            return false;
        }
        else
            $('#message').css("visibility", "hidden")

        updateView();
    });
});

function retrieveFormDigest() {
    var contextInfoUri = addinweburl + "/_api/contextinfo";
    var executor = new SP.RequestExecutor(addinweburl);

    executor.executeAsync({
        url: contextInfoUri,
        method: "POST",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            var jsonObject = JSON.parse(data.body);
            formDigestValue = jsonObject.d.GetContextWebInformation.FormDigestValue;
            getEmployeeNames();
        },
        error: function (data, errorCode, errorMessage) {
            var errMsg = "Error retrieving the form digest value: "
                + errorMessage;
            $("#error").text(errMsg);
        }
    });
}

function validateDate() {
    if (($('input#dtFrom').val() != '' && $('input#dtTo').val() != '') && ($('input#dtFrom').val() > $('input#dtTo').val())) {
        $('#message').css("visibility", "visible")
        return false;
    }
}

function getEmployeeNames() {
    var executor = new SP.RequestExecutor(addinweburl);

    executor.executeAsync({
        url: addinweburl +
      "/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Professional Development Form')/items?$select=Display_x0020_Name&$orderby=Display_x0020_Name asc&@target='" + hostweburl + "'",
        method: "GET",
        headers: {
            "accept": "application/json; odata=verbose",
            "X-RequestDigest": formDigestValue,
        },
        success: gdnSuccessHandler,
        error: gdnFailHandler
    });
}

function gdnSuccessHandler(data) {
    var body = JSON.parse(data.body);
    var result = body.d.results;

    var displayNames = [];
    for (var i = 0; i < result.length; i++) {
        displayNames.push(result[i].Display_x0020_Name);
    }
    var distinctDisplayNames = displayNames.filter(function (itm, i, displayNames) {
        return i == displayNames.indexOf(itm);
    });

    for (var i = 0; i < distinctDisplayNames.length; i++) {
        $("#lstDisplayName").append("<option value='" + distinctDisplayNames[i] + "'>" + distinctDisplayNames[i] + "</option>");
    }
}

function gdnFailHandler(data, errorCode, errorMessage) {
    console.log("Could not complete creating View: " + errorMessage);
}

function updateView() {
    var disName = $('#lstDisplayName').val();

    var lastYear = (new Date()).getFullYear() - 1;
    var stDate = "10/01/" + lastYear;
    var startDate = (new Date(stDate)).toISOString();

    var dateFromQuery = ($('input#dtFrom').val() == '') ? "<Geq><FieldRef Name='Created' /><Value Type='DateTime'>" + startDate + "</Value></Geq>" : "<Geq><FieldRef Name='Created' /><Value Type='DateTime'>" + (new Date($('input#dtFrom').val())).toISOString() + "</Value></Geq>";
    var dateToQuery = ($('input#dtTo').val() == '') ? "<Leq><FieldRef Name='Created' /><Value Type='DateTime'><Today/></Value></Leq>" : "<Leq><FieldRef Name='Created' /><Value Type='DateTime'>" + (new Date($('input#dtTo').val())).toISOString() + "</Value></Leq>";
    //var approvedQuery = "<And><And><Eq><FieldRef Name='Mgr_x0020_App_x0020_Dis'/><Value Type='Boolean'>1</Value></Eq><Eq><FieldRef Name='PC_x0020_App_x0020_Diss'/><Value Type='Boolean'>1</Value></Eq></And><Eq><FieldRef Name='Pre_x0020_Approved'/><Value Type='Boolean'>1</Value></Eq></And>";
    //var query = "'<OrderBy><FieldRef Name='Last_x0020_Name' Ascending='TRUE'/></OrderBy><Where><And><And>" + dateFromQuery + dateToQuery + "</And>" + approvedQuery + "</And></Where>'";

    //Test query
    var approvedQuery = "<And><And><Eq><FieldRef Name='Mgr_x0020_App_x0020_Dis'/><Value Type='Boolean'>0</Value></Eq><Eq><FieldRef Name='PC_x0020_App_x0020_Diss'/><Value Type='Boolean'>0</Value></Eq></And><Eq><FieldRef Name='Pre_x0020_Approved'/><Value Type='Boolean'>0</Value></Eq></And>";
    var query = "'<Where><And>" + dateFromQuery + dateToQuery + "</And></Where><OrderBy><FieldRef Name='Last_x0020_Name'/></OrderBy>'";

    if (disName != 'Please select')
        query = "'<OrderBy><FieldRef Name='Last_x0020_Name' Ascending='TRUE'/></OrderBy>" +
                "<Where><And><And>" + dateFromQuery + dateToQuery + "</And><Eq><FieldRef Name='Display_x0020_Name' />" +
               "<Value Type='Text'>" + disName + "</Value></Eq></And>" + approvedQuery + "</Where>'";

    var executor = new SP.RequestExecutor(addinweburl);
    executor.executeAsync({
        url: addinweburl +
            "/_api/SP.AppContextSite(@target)/web/Lists/getbytitle('Professional Development Form')/getitems?@target='" + hostweburl + "'",
        method: "POST",
        body: "{ 'query' : {'__metadata': { 'type': 'SP.CamlQuery' }, \"ViewXml\": \"<View><Query>" + query + "</Where></Query></View>\" } }",
        headers: {
            "accept": "application/json; odata=verbose",
            "content-type": "application/json;odata=verbose"
        },
        success: uvSuccessHandler,
        error: uvFailHandler
    });
}

function groupBy(array, f) {
    var groups = {};
    array.forEach(function (o) {
        var group = JSON.stringify(f(o));
        groups[group] = groups[group] || [];
        groups[group].push(o);
    });
    return Object.keys(groups).map(function (group) {
        return groups[group];
    })
}

function uvSuccessHandler(data) {
    if (data.body == "") {
        return false;
    }
    var body = JSON.parse(data.body);
    var result = [];
    result = groupBy(body.d.results, function (item) {
        return [item.Display_x0020_Name];
    });

    perDiem = [];
    reconRepAdj = [];
    registrationCost = [];
    hotelCost = [];
    travelCost = [];
    totalCost = [];
    totalPerPerson = {};
    var header;
    var totalRow;
    rows = "";
    $.each(result, function (index, value) {
        formReport(value);
    });

    header = "<tr><th>Last Name</th><th>First Name</th><th>Conference Title</th><th>Dt From</th><th>Dt To</th>";

    //Add Header and total values for each field
    totalRow = "<tr class='columntotal'><td colspan='5'></td>";
    if ($('#ckbPerDiem').is(':checked')) {
        header += "<th>Per Diem</th>";
        totalRow += "<td class='bold'>$" + getTotal(perDiem) + "</td>";
    }
    if ($('#ckbReconciliation').is(':checked')) {
        header += "<th>Reconciliation Report Adjustment</th>";
        totalRow += "<td class='bold'>$" + getTotal(reconRepAdj) + "</td>";
    }
    if ($('#ckbRegistration').is(':checked')) {
        header += "<th>Registration Cost</th>";
        totalRow += "<td class='bold'>$" + getTotal(registrationCost) + "</td>";
    }
    if ($('#ckbHotel').is(':checked')) {
        header += "<th>Total Hotel Cost</th>";
        totalRow += "<td class='bold'>$" + getTotal(hotelCost) + "</td>";
    }
    if ($('#ckbTravel').is(':checked')) {
        header += "<th>Cost of Mode of Travel</th>";
        totalRow += "<td class='bold'>$" + getTotal(travelCost) + "</td>";
    }

    header += "<th>Total cost per person</th></tr>";
    totalRow += "<td class='bold'>$" + getTotal(totalCost) + "</td></tr>";

    reportContent = "<div id=\"contentToPrint\">" +
                       "<div><div class='headerBkgd'><div class=\"siteLogo\"><img id=\"logo\" src=\"../Images/OPC-new-logo.jpg\" alt=\"Office of People's Counsel\" class=\"logo\"></div>" +
                       "<div class=\"pageTitle\"> <h2>OPC Agency FY Training Expense</h2></div></div><div class=\"reportDate\">Report Date: " + (new Date()).format("MM/dd/yyyy") + "</div>" +
                        "<table id=\"tblReport\">" + header + rows + totalRow + "</table>" +
                    "</div><br/><br/><input type='button' id='btnPrint' value='Print' onclick=\"printReport()\">";
    openReport();
}

function formReport(result) {
    var showPerDiem = $('#ckbPerDiem').is(':checked');
    var showReconciliation = $('#ckbReconciliation').is(':checked');
    var showRegistration = $('#ckbRegistration').is(':checked');
    var showHotel = $('#ckbHotel').is(':checked');
    var showTravel = $('#ckbTravel').is(':checked');
    var costOfTraining = [];

    for (var i = 0; i < result.length; i++) {
        var costPerPerson = [];
        var dateFrom = (new Date(result[i].Dt_x0020_From)).format("MM/dd/yyyy");
        var dateTo = (new Date(result[i].Dt_x0020_To)).format("MM/dd/yyyy");
        var conferenceTitle = "";
        if (result[i].Conference_x0020_Title != null)
            conferenceTitle = result[i].Conference_x0020_Title.toString();

        rows += "<tr><td>" + result[i].Last_x0020_Name + "</td><td>" + result[i].First_x0020_Name + "</td><td>" + conferenceTitle + "</td><td>" + dateFrom + "</td><td>" + dateTo + "</td>";

        if (showPerDiem) {
            rows += "<td>$" + round2Fixed(result[i].Per_x0020_Diem_x0020_Sub_x0020_Total) + "</td>";
            costPerPerson.push(result[i].Per_x0020_Diem_x0020_Sub_x0020_Total);
            perDiem.push(result[i].Per_x0020_Diem_x0020_Sub_x0020_Total);
        }
        if (showReconciliation) {
            rows += "<td>$" + round2Fixed(result[i].Reconciliation_x0020_Report_x0020_Adjustment) + "</td>";
            costPerPerson.push(result[i].Reconciliation_x0020_Report_x0020_Adjustment);
            reconRepAdj.push(result[i].Reconciliation_x0020_Report_x0020_Adjustment);
        }
        if (showRegistration) {
            rows += "<td>$" + round2Fixed(result[i].Registration_x0020_Cost) + "</td>";
            costPerPerson.push(result[i].Registration_x0020_Cost);
            registrationCost.push(result[i].Registration_x0020_Cost);
        }
        if (showHotel) {
            rows += "<td>$" + round2Fixed(result[i].Txt_x0020_Hotel_x0020_Total_x0020_Cost) + "</td>";
            costPerPerson.push(result[i].Txt_x0020_Hotel_x0020_Total_x0020_Cost);
            hotelCost.push(result[i].Txt_x0020_Hotel_x0020_Total_x0020_Cost);
        }
        if (showTravel) {
            rows += "<td>$" + round2Fixed(result[i].Travel_x0020_Cost) + "</td>";
            costPerPerson.push(result[i].Travel_x0020_Cost);
            travelCost.push(result[i].Travel_x0020_Cost);
        }

        var totalCostPerPerson = getTotal(costPerPerson);
        rows += (result.length == 1) ? "<td class='bold'>$" + totalCostPerPerson + "</td></tr>" : "<td>$" + totalCostPerPerson + "</td></tr>";
        totalCost.push(totalCostPerPerson);
        costOfTraining.push(totalCostPerPerson);
    }
    //5 fields will always be shown 
    var activeFields = showPerDiem + showReconciliation + showRegistration + showHotel + showTravel + 5;

    if (result.length > 1)
        rows += "<tr><td colspan='" + activeFields + "' style=\"height: 15px;\" ></td><td class='bold'>$" + getTotal(costOfTraining) + "</td></tr>";

    rows += "<tr><td colspan='" + activeFields + "' style=\"height: 15px;\" ></td><td></td></tr>"
}

function uvFailHandler(data, errorCode, errorMessage) {
    console.log("Could not complete creating View: " + errorMessage);
}

function openReport() {
    var newWin = window.open("", "_blank");
    setTimeout(function () { newWin; }, 10);
    newWin.document.open();
    newWin.document.write('<html><head></head><body>' + reportContent + '</body></html>');

    var link = document.createElement('link');
    link.href = '../Content/Report.css';
    link.type = 'text/css';
    link.rel = 'Stylesheet';
    newWin.document.getElementsByTagName('head')[0].appendChild(link);

    var jqscript = document.createElement('script');
    jqscript.src = '../Scripts/jquery-1.12.4.min.js';
    jqscript.type = 'text/javascript';
    newWin.document.getElementsByTagName('head')[0].appendChild(jqscript);

    var script = document.createElement('script');
    script.src = '../Scripts/PrintReport.js';
    script.type = 'text/javascript';
    newWin.document.getElementsByTagName('head')[0].appendChild(script);

    newWin.document.title = "Training Expense Report";
}

function formatNumber(amount) {
    if (amount && amount != 0) {
        var currency = "$" + amount.toFixed(2);
    }
}

function getQueryStringParameter(paramToRetrieve) {
    var params =
        document.URL.split("?")[1].split("&");
    var strParams = "";
    for (var i = 0; i < params.length; i = i + 1) {
        var singleParam = params[i].split("=");
        if (singleParam[0] == paramToRetrieve)
            return singleParam[1];
    }
}

function round2Fixed(value) {
    value = +value;

    if (isNaN(value))
        return NaN;

    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + 2) : 2)));

    value = value.toString().split('e');
    return (+(value[0] + 'e' + (value[1] ? (+value[1] - 2) : -2))).toFixed(2);
}

function getTotal(itemsToSum) {
    var sum = 0;
    $(itemsToSum).each(function () {
        var str = this;
        if (str) {
            sum += Number(str)
        }
    });
    return round2Fixed(sum);
}